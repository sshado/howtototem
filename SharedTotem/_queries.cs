﻿using System;
using System.Collections.Generic;
using System.Text;
using Totem;
using Totem.Timeline;

namespace SharedTotem
{
    public class UserQuery : Query
    {
        public Id Id;
        public Id Name;
        public string Alias;

        static Id RouteFirst(UserCreated e) => e.Id;

        void Given(UserCreated e)
        {
            Id = e.Id;
            Name = e.Name;
            Alias = e.Alias;
        }

    }

    public class UserCreated : Event
    {
        public Id Id = Id.From(Guid.NewGuid());
        public Id Name;
        public string Alias;
    }
}
