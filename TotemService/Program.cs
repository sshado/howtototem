﻿using Microsoft.Extensions.DependencyInjection;
using SharedTotem;
using System;
using System.Threading.Tasks;
using Totem.App.Service;
using Totem.Timeline.SignalR.Hosting;

namespace TotemService
{
    class Program
    {
        static ConfigureServiceApp Configuration = new ConfigureServiceApp();

        public static Task Main(string[] args)
        {
            return ServiceApp.Run<SharedArea>(Configuration
                .AfterServices(services => 
                {
                    services.AddSignalR().AddQueryNotifications();
                }));
        }
    }
}
