﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SharedTotem;
using Totem.App.Web;
using Totem.Timeline.SignalR.Hosting;

namespace TotemApp1
{
    public class Program
    {
        static ConfigureWebApp Configuration = new ConfigureWebApp();
        static string webRoot = Path.Combine(AppContext.BaseDirectory, "wwwroot");

        public static Task Main(string[] args)
        {
            return WebApp.Run<SharedArea>(Configuration
                .BeforeHost(host => host.UseWebRoot(webRoot))
                .AfterApp(builder =>
                {
                    Configuration.BeforeSignalRApp(app =>
                    {
                        app.UseSignalR((srConfig) =>
                        {
                            srConfig.MapQueryHub(options =>
                            {
                                options.Transports = HttpTransportType.WebSockets;
                            });
                        });
                    });
                })
                .AfterServices(services => Startup.ConfigureServices(services)));
        }
    }
}
