import Timeline from 'totem-timeline';

export default {
    webUser: Timeline.webQuery("apiEndpoint", args => args.apiEndpoint, (options) => {
        return {
            headers: {
                //"Authorization": `Bearer token`,
                "Connection": `keep-alive`
            }
        }
    })
}