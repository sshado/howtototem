﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharedTotem;
using Totem;
using Totem.Timeline.Mvc;

namespace TotemApp1.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class ViewsController : ControllerBase
    {
        public ICommandServer _commands;
        public IQueryServer _queries;

        public ViewsController(ICommandServer commands, IQueryServer queries)
        {
            _commands = commands;
            _queries = queries;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> User()
        {
            var query = await _queries.Get<UserQuery>(Id.From("root"));
            return query;
        }
    }
}
